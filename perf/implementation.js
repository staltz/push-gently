var usage = require('cpu-percentage');
var AsyncStream = require('push-stream/throughs/async');

module.exports = function gently(opts) {
  var _opts = opts || {ceiling: 88, wait: 144, maxPause: Infinity};
  var ceiling = _opts.ceiling || 88;
  var wait = _opts.wait || 144;
  var maxPause = _opts.maxPause || Infinity;
  var start = (stats = usage());
  var lastResume = Date.now();
  var step = 2,
    i = 0;

  return new AsyncStream(function (data, cb) {
    if (i++ % step) return cb(null, data);

    stats = usage(start);
    process.stdout.write(`${stats.time},${stats.percent}\n`);
    if (
      stats.percent < ceiling || // "CPU is cold enough"
      Date.now() - lastResume > maxPause || // "remained paused for too long"
      stats.time < 20 // "we just now began draining"
    ) {
      step = step << 1 || 1;
      lastResume = Date.now();
      cb(null, data);
    } else {
      step = step >> 1 || 1;
      setTimeout(cb, wait, null, data);
    }
  });
};
