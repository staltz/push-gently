var usage = require('cpu-percentage');
var drain = require('push-stream/sinks/drain');
var source = require('./source');

var start = usage();
process.stdout.write(`"time","cpu_percent"\n`);
process.stdout.write(`${start.time},${start.percent}\n`);
var count = 0;

source.pipe(
  drain(
    () => {
      count += 1;
      if (count % 10000 === 0) {
        const stats = usage(start);
        process.stdout.write(`${stats.time},${stats.percent}\n`);
      }
    },
    () => {
      const stats = usage(start);
      process.stdout.write(`${stats.time},${stats.percent}\n`);
    },
  ),
);
