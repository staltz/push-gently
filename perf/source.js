var fs = require('fs');
var ValuesStream = require('push-stream/sources/values');
var AsyncStream = require('push-stream/throughs/async');

module.exports = new ValuesStream(Array(9e5)).pipe(
  new AsyncStream((x, cb) =>
    fs.readFile('../package.json', (err, contents) => {
      cb(
        err,
        Buffer.from(contents.toString('hex').toUpperCase())
          .toString('base64')
          .toLowerCase()
          .substr(0, 10 + Math.round(Math.random() * 100)),
      );
    }),
  ),
);
