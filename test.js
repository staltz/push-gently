var test = require('tape');
var usage = require('cpu-percentage');
var fs = require('fs');
var ValuesStream = require('push-stream/sources/values');
var CollectStream = require('push-stream/sinks/collect');
var AsyncStream = require('push-stream/throughs/async');
var gently = require('./index');

test('basic', function (t) {
  new ValuesStream([4, 5, 6]).pipe(gently()).pipe(
    new CollectStream(function (err, ary) {
      t.error(err, 'no err');
      t.deepEqual(ary, [4, 5, 6]);
      t.end();
    }),
  );
});

test('respects `maxPause` opt', function (t) {
  t.plan(1);
  t.timeoutAfter(8000);

  const ceiling = 0.01; // super low threshold, most likely never met
  const maxPause = 50; // ms

  new ValuesStream(Array(200))
    .pipe(
      new AsyncStream((x, cb) =>
        fs.readFile('./package.json', (err, contents) => {
          cb(
            err,
            Buffer.from(contents.toString('hex').toUpperCase())
              .toString('base64')
              .toLowerCase()
              .substr(0, 20),
          );
        }),
      ),
    )
    .pipe(gently({ceiling, maxPause, wait: 16}))
    .pipe(
      new CollectStream(function (err, ary) {
        t.pass('all data points processed');
        t.end();
      }),
    );
});

test('heavy processing is below the prescribed CPU 90% ceiling', function (t) {
  t.plan(2);
  t.timeoutAfter(20e3);

  const ceiling = 90;

  const start = usage();
  new ValuesStream(Array(20000))
    .pipe(
      new AsyncStream((x, cb) =>
        fs.readFile('./package.json', (err, contents) => {
          cb(
            err,
            Buffer.from(contents.toString('hex').toUpperCase())
              .toString('base64')
              .toLowerCase()
              .substr(0, 20),
          );
        }),
      ),
    )
    .pipe(gently({ceiling}))
    .pipe(
      new CollectStream(function (err, ary) {
        t.error(err, 'no err');
        const stats = usage(start);
        console.log(usage(start));
        t.true(stats.percent < ceiling + 3, 'CPU usage was approx. 90%');
      }),
    );
});

test('heavy processing is below the prescribed CPU 50% ceiling', function (t) {
  t.plan(2);
  t.timeoutAfter(20e3);

  const ceiling = 50;

  const start = usage();
  new ValuesStream(Array(8000))
    .pipe(
      new AsyncStream((x, cb) =>
        fs.readFile('./package.json', (err, contents) => {
          cb(
            err,
            Buffer.from(contents.toString('hex').toUpperCase())
              .toString('base64')
              .toLowerCase()
              .substr(0, 20),
          );
        }),
      ),
    )
    .pipe(gently({ceiling}))
    .pipe(
      new CollectStream(function (err, ary) {
        t.error(err, 'no err');
        const stats = usage(start);
        console.log(usage(start));
        t.true(stats.percent < ceiling + 3, 'CPU usage was approx. 50%');
      }),
    );
});
