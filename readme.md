# push-gently

> A push-stream through that applies backpressure when CPU is too busy

```bash
npm install --save push-gently
```

Similar to [pull-drain-gently](https://gitlab.com/staltz/pull-drain-gently), but for push-streams.

## Usage

### Default use

```js
const gently = require('push-gently');

myPushStream
  .pipe(gently())
  .pipe({
    paused: false,
    write: function (x) {
      console.log(x);
    },
  });
```

### Tweaking the parameters

This push-stream through listens to the source, but once in a while it will check the CPU usage, and if it has gone above the limit known as the `ceiling`, it pauses itself for `wait` milliseconds, and then resumes the source, in such a way that it should stay close to the limit. In other words, two parameters control the draining:

- `ceiling`: the maximum CPU usage you want the Node.js process to consume, approximately, in percentages (`100` is 100%, not `1`)
- `wait`: the waiting period, in milliseconds, to pause draining in order to allow other tasks to use the CPU

The default CPU ceiling is `88%` and the default waiting period for each pause is `144ms` (roughly 9 frames if you have the UI running at 60fps). This means that the longest pause during which no drains will occur is 144ms.

There is also a third less common parameter, which by default is turned off:

- `maxPause`: a limit in milliseconds for how long to pause. For instance, when `maxPause = 5000`, the draining will remain paused for 5 seconds maximum, after that it will d resume draining _regardless_ of the current CPU usage. **The default is `Infinity`**.

To configure your own parameters, pass an `opts` object as the argument to `gently`:

```js
const gently = require('push-gently');

myPushStream
  .pipe(gently({ ceiling: 90, wait: 60 })) // <--- opts
  .pipe({
    paused: false,
    write: function (x) {
      console.log(x);
    },
  });
```

To configure these parameters, consider that:

- The **greater the `ceiling`** is, the closer the pipeline behaves to stock `push.drain`, i.e. the _less gently_ it drains with regards to CPU load
- The **smaller the `ceiling`** is, the more time it will take to consume the source, i.e. the slower your application will finish its CPU tasks
- The **greater the `wait`**, the more the actual CPU usage fluctuates below and above the `ceiling`, i.e. the more bumpy the ride is for CPU usage and workload throughput
- The **smaller the `wait`**, the more the actual CPU usage accurately meets the `ceiling`, but also the more overhead there is with many short-lived timers for those pauses

The total time for drainage is also important. `push.drain` without `gently` is the fastest, having the shortest total time. `gently` with a small `wait` might give a total drainage time of approx. 2.5x that of `push.drain`.

The defaults `ceiling=88`, `wait=144` are a sweet spot, and it can achieve a total drainage time of approx 1.4x that of `push.drain`. You can run benchmarks yourself by running `cd perf && ./run-all.sh` in this repository.

The table below shows how the JS CPU flamechart looks like when running the benchmark in `perf` on Ubuntu 18.04.3 x86_64, Intel® Core™ i7-7500U CPU @ 2.70GHz × 4, 15,4 GiB RAM, for different values of the `ceiling` parameter. "Unlimited" means `push.drain` without `gently`:

| CPU `ceiling` | CPU Profiler in Chrome |
|-----------|------------------------|
| Unlimited | ![100](./images/100.png) |
| `90`% | ![90](./images/90.png) |
| `80`% | ![80](./images/80.png) |
| `70`% | ![70](./images/70.png) |
| `60`% | ![60](./images/60.png) |
| `50`% | ![50](./images/50.png) |
| `40`% | ![40](./images/40.png) |

## License

[MIT](https://tldrlegal.com/license/mit-license)
